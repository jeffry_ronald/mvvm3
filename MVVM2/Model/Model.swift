

import Foundation

// MARK: - Model Support

struct updatedDate {
    
    let month:  Int
    let day:    Int
    let year:   Int
    
}

// MARK: - Model

struct Karyawan1DataModel {
    
    let formalName: String  // "Karyawan" #
    let commonName: String  // common name
    let pageLink:   String  // NASA overview page
    let imageLink:  String  // NASA detail image link
    let updateDate: updatedDate // NASA page updated date
    let description: String // Karyawan object description
    let thumbnail:  String  // placeholder for detail image
    
}

// MARK: - Model Data



let updateDateKaryawan1 = updatedDate(month: 10, day: 19, year: 2017)

let Karyawan1 = Karyawan1DataModel(formalName: "Karyawan 1", commonName: "Bagong", pageLink: "https://www.nasa.gov/feature/goddard/2017/messier-1-the-crab-nebula", imageLink: "https://www.nasa.gov/sites/default/files/thumbnails/image/crab-nebula-mosaic.jpg", updateDate: updateDateKaryawan1, description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: "telescope")

let updateDateKaryawan8 = updatedDate(month: 10, day: 19, year: 2017)

let Karyawan8 = Karyawan1DataModel(formalName: "Karyawan 8", commonName: "Codot", pageLink: "https://www.nasa.gov/feature/goddard/2017/messier-8-the-lagoon-nebula", imageLink: "https://www.nasa.gov/sites/default/files/thumbnails/image/heic1015a.jpg", updateDate: updateDateKaryawan8, description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: "telescope")

let updateDateKaryawan57 = updatedDate(month: 10, day: 19, year: 2017)

let Karyawan57 = Karyawan1DataModel(formalName: "Karyawan 57", commonName: "Agus", pageLink: "https://www.nasa.gov/feature/goddard/2017/messier-57-the-ring-nebula", imageLink: "https://www.nasa.gov/sites/default/files/thumbnails/image/ring-nebula-full_jpg.jpg", updateDate: updateDateKaryawan57, description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: "telescope")



