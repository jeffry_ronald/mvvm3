


import Foundation

// #1 - "Should I Stay or Should I Go"
// - The Clash
import UIKit

/**
 #2 - Define a closure TYPE for updating a UIImageView once an image downloads.
 
 - parameter imageData: raw NSData making up the image
 */
public typealias ImageDownloadCompletionClosure = (_ imageData: NSData ) -> Void

// MARK: - #3 - App data through ViewModel

var karyawanViewModel: [KaryawanViewModel] =
    [KaryawanViewModel(karyawanDataModel: Karyawan1),
     KaryawanViewModel(karyawanDataModel: Karyawan8),
     KaryawanViewModel(karyawanDataModel: Karyawan57)]

// MARK: - #4 - View Model

class KaryawanViewModel
{
    
    // #5 - I use some private properties solely for
    // preparing data for presentation in the UI.
    
    private let karyawanDataModel: Karyawan1DataModel
    
    private var imageURL: URL
    
    private var updatedDate: Date?
    
    init(karyawanDataModel: Karyawan1DataModel)
    {
        self.karyawanDataModel = karyawanDataModel
        self.imageURL = URL(string: karyawanDataModel.imageLink)!
    }
    
    // #6 - Data is made available for presentation only
    // through public getters. No direct access to Model.
    // Some getters prepare data for presentation.

    public var formalName: String {
        return "Formal name: " + karyawanDataModel.formalName
    }
    
    public var commonName: String {
        return "Common name: " + karyawanDataModel.commonName
    }
    
    public var dateUpdated: String {
        
        let dateString = String(karyawanDataModel.updateDate.year) + "-" +
                         String(karyawanDataModel.updateDate.month) + "-" +
                         String(karyawanDataModel.updateDate.day)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMMM dd, yyyy"
        
        if let date = dateFormatterGet.date(from: dateString) {
            updatedDate = date
            return "Updated: " + dateFormatterPrint.string(from: date)
        }
        else {
            return "There was an error decoding the string"
        }
    }
    
    // #7 - Controversial? Should this SOLELY live in the UI?
    public var textDescription: NSAttributedString {
        
        let fontAttributes = [NSAttributedString.Key.font:  UIFont(name: "Georgia", size: 14.0)!, NSAttributedString.Key.foregroundColor: UIColor.blue]
        let markedUpDescription = NSAttributedString(string: karyawanDataModel.description, attributes:fontAttributes)
        return markedUpDescription
        
    }
    
    public var thumbnail: String {
        return karyawanDataModel.thumbnail
    }
    
    // #8 - Controversial? Is passing a completion handler into the view
    // model problematic? Should I use KVO or delegation? All's I'm
    // doing is getting some NSData/Data.
    func download(completionHanlder: @escaping ImageDownloadCompletionClosure)
    {
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:imageURL)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            
            if let tempLocalUrl = tempLocalUrl, error == nil {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    let rawImageData = NSData(contentsOf: tempLocalUrl)
                    completionHanlder(rawImageData!)
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
            } else {
                print("Error took place while downloading a file. Error description: \(String(describing: error?.localizedDescription))")
            }
        } // end let task
        
        task.resume()
        
    } // end func download

} // end class MessierViewModel
